package com.javacop.apitraining.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Main application configuration.
 * 
 * @author borjabi
 *
 */
@Configuration
@EnableConfigurationProperties
public class ApplicationConfig {

	
}
