package com.javacop.apitraining.config;

import org.springframework.stereotype.Component;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Test configuration example. 
 * 
 * @author borjabi
 *
 */
//@Component
//@ConfigurationProperties(locations = "file:${com.javacop.config.path}/appEnvironmentConfig.yml",  prefix = "server")
public class TestConfig {
	
	/** Test name.*/
	private String name;
	
	/** Test URL. */
	private String url;

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
