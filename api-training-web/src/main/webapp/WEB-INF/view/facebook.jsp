<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="test page">
    <meta name="author" content="Java CoP">

    <title>API design and development</title>

	<!-- CSS -->
	<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
    <spring:url value="/resources/core/css/animate.min.css" var="animateCss" />
    <spring:url value="/resources/core/css/creative.css" var="creativeCss" />
    <spring:url value="/resources/core/font-awesome/css/font-awesome.min.css" var="fontAwesomeMinCss" />
    
    <!-- JS -->
    <spring:url value="/resources/core/js/jquery.js" var="jqueryJs" />
    <spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapMinJs" />
    <spring:url value="/resources/core/js/jquery.easing.min.js" var="easingMinJs" />
    <spring:url value="/resources/core/js/jquery.fittext.js" var="fittextJs" />
    <spring:url value="/resources/core/js/wow.min.js" var="wowMinJs" />
    <spring:url value="/resources/core/js/creative.js" var="creativeJs" />
    
    <!-- IMG -->
    <spring:url value="/resources/core/img/portfolio/1.png" var="portafolio1Img" />
    <spring:url value="/resources/core/img/portfolio/2.jpg" var="portafolio2Img" />
    <spring:url value="/resources/core/img/portfolio/3.jpg" var="portafolio3Img" />
    <spring:url value="/resources/core/img/portfolio/4.jpg" var="portafolio4Img" />
    <spring:url value="/resources/core/img/portfolio/5.jpg" var="portafolio5Img" />
    <spring:url value="/resources/core/img/portfolio/6.jpg" var="portafolio6Img" />

    <!-- Bootstrap Core CSS -->
    <link href="${bootstrapCss}" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="${fontAwesomeMinCss}" type="text/css">

    <!-- Plugin CSS -->
    <link href="${animateCss}" rel="stylesheet" />

    <!-- Custom CSS -->
   <link href="${creativeCss}" rel="stylesheet" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">

	<script>
		//Init Facebook Login
		window.fbAsyncInit = function() {
			console.log("initMethod");
		    FB.init({
		      appId      : '135585086985857',
		      xfbml      : true,
		      version    : 'v2.5'
		    });
		    
		    //Checkin ghe status 
		    checkLoginState();
		};
		
		//Checking status
		function checkLoginState(){
			console.log("checkFacebookStatus");
			FB.getLoginStatus(function(response) {
				console.log(response);
				if (response.status === 'connected') {
					loadProfile();
					$('#login').hide();
					$('#profile').show();
				} else {
					$('#profile').hide();
					$('#login').show();
				}
			});
		}
		
		// Method that loads profile information from Facebook and display into the screen.
		function loadProfile() {
			console.log('Welcome!  Fetching your information.... ');
			
			FB.api('/me?fields=name,birthday,age_range,email,cover,about,picture,hometown',
					function(response) {
						console.log('Successful login for: '+ response.name);
						console.log(response);
						$('#username').text("Name: " + response.name);
						$('#dob').text("Birthday: " + response.birthday);
						$('#email').text("Email: " + response.email);
						$('#hometown').text("Hometown: " + response.hometown.name);
						$('#about').text("About you: " + response.about);
						$('#picture').attr('src',response.picture.data.url);
					});
			
			
			
		}

		// Load the SDK asynchronously
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id))
				return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="/#page-top">Home</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="/#code">Source Code</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/#materials">Materials</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/#demos">Demos</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
  
    <section class="bg-primary" id="code">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Demo 1 - Facebook API Example.</h2>
                    <hr class="light">
                    <p class="text-faded">The objective of this demo is just show a simple example of a well known Rest API designed to integrate applications with Facebook.</p>
                </div>
            </div>
        </div>
    </section>

    <section id="login">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Let's see how works!</h2>
                    <hr class="primary">
                    <p id="">Just login using you Facebook account. </p>
                </div>
                <div class="col-lg-12 text-center">
                    <div class="fb-login-button" onlogin="checkLoginState();" data-scope="public_profile, email, user_birthday, user_status, user_about_me, user_hometown" data-max-rows="1" data-size="large" data-button-type="login_with" 
								data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>
                </div>
               
            </div>
        </div>
    </section>
    
    <section id="profile">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Let's see your profile now!</h2>
                    <hr class="primary">
                    <p id="username"></p>
                    <p id="dob"></p>
                    <p id="email"></p>
                    <p id="hometown"></p>
                    <p id="about"></p>
                    <img id="picture" src=""/>
                </div>
               
            </div>
        </div>
    </section>
    
    <section class="bg-primary" id="code">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Source code.</h2>
                    <hr class="light">
                    <p class="text-faded">The code of this demo is available in Bitbucket. Click in the link below to access to the GIT repository.</p>
                    <a href="https://bitbucket.org/account/user/javacop/projects/AT" target="_blank" class="btn btn-default btn-xl">Go to Bitbucket</a>
                </div>
            </div>
        </div>
    </section>
    
    <section id="links">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Links.</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 text-center">
                    <div class="service-box">
                        <h3><a href="https://developers.facebook.com/docs/facebook-login" target="_blank">Facebook Login.</a></h3>
                        <p class="text-muted"><a href="https://developers.facebook.com/docs/facebook-login" target="_blank">Description of API provided by Facebook to obtain a token.</a></p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 text-center">
                    <div class="service-box">
                        <h3><a href="https://developers.facebook.com/docs/graph-api" target="_blank">The Graph API</a></h3>
                        <p class="text-muted"><a href="https://developers.facebook.com/docs/graph-api" target="_blank">Description of how use the Facebook API.</a></p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 text-center">
                    <div class="service-box">
                        <h3><a href="https://developers.facebook.com/tools/explorer/" target="_blank">Graph API Explorer</a></h3>
                        <p class="text-muted"><a href="https://developers.facebook.com/tools/explorer/" target="_blank">Tool provided by Facebook to test the API.</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- jQuery -->
    <script src="${jqueryJs}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="${bootstrapMinJs}"></script>

    <!-- Plugin JavaScript -->
    <script src="${easingMinJs}"></script>
    <script src="${fittextJs}"></script>
    <script src="${wowMinJs}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="${creativeJs}"></script>

</body>

</html>
