<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="test page">
    <meta name="author" content="Java CoP">

    <title>API design and development</title>

	<!-- CSS -->
	<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
    <spring:url value="/resources/core/css/animate.min.css" var="animateCss" />
    <spring:url value="/resources/core/css/creative.css" var="creativeCss" />
    <spring:url value="/resources/core/font-awesome/css/font-awesome.min.css" var="fontAwesomeMinCss" />
    
    <!-- JS -->
    <spring:url value="/resources/core/js/jquery.js" var="jqueryJs" />
    <spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapMinJs" />
    <spring:url value="/resources/core/js/jquery.easing.min.js" var="easingMinJs" />
    <spring:url value="/resources/core/js/jquery.fittext.js" var="fittextJs" />
    <spring:url value="/resources/core/js/wow.min.js" var="wowMinJs" />
    <spring:url value="/resources/core/js/creative.js" var="creativeJs" />
    
    <!-- IMG -->
    <spring:url value="/resources/core/img/portfolio/1.png" var="portafolio1Img" />
    <spring:url value="/resources/core/img/portfolio/2.jpg" var="portafolio2Img" />
    <spring:url value="/resources/core/img/portfolio/3.jpg" var="portafolio3Img" />
    <spring:url value="/resources/core/img/portfolio/4.jpg" var="portafolio4Img" />
    <spring:url value="/resources/core/img/portfolio/5.jpg" var="portafolio5Img" />
    <spring:url value="/resources/core/img/portfolio/6.jpg" var="portafolio6Img" />

    <!-- Bootstrap Core CSS -->
    <link href="${bootstrapCss}" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="${fontAwesomeMinCss}" type="text/css">

    <!-- Plugin CSS -->
    <link href="${animateCss}" rel="stylesheet" />

    <!-- Custom CSS -->
   <link href="${creativeCss}" rel="stylesheet" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">Home</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="#code">Source Code</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#materials">Materials</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#demos">Demos</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1>API design and development</h1>
                <hr>
                <p>Welcome to the site of the API design and development. Here you will find all materials and samples presented in the sessions.</p>
            </div>
        </div>
    </header>

    <section class="bg-primary" id="code">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Source code.</h2>
                    <hr class="light">
                    <p class="text-faded">All developed demos for this training, including this web page, are available in Bitbucket. Click in the link below to access to the GIT repository.</p>
                    <a href="https://bitbucket.org/account/user/javacop/projects/AT" target="_blank" class="btn btn-default btn-xl">Go to Bitbucket</a>
                </div>
            </div>
        </div>
    </section>

    <section id="materials">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Materials</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-diamond wow bounceIn text-primary"></i>
                        <h3><a href="/resources/downloads/API_Training_Event_Session_1.pdf" target="_blank">Session 1</a></h3>
                        <p class="text-muted"><a href="/resources/downloads/API_Training_Event_Session_1.pdf" target="_blank">API design introduction.</a></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-paper-plane wow bounceIn text-primary" data-wow-delay=".1s"></i>
                        <h3>Session 2</h3>
                        <p class="text-muted">No developed yet, but it will be something interesting!</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-newspaper-o wow bounceIn text-primary" data-wow-delay=".2s"></i>
                        <h3>Session 3</h3>
                        <p class="text-muted">No developed yet, but it will be something interesting!</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-heart wow bounceIn text-primary" data-wow-delay=".3s"></i>
                        <h3>Session 4</h3>
                        <p class="text-muted">No developed yet, but it will be something interesting!</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="no-padding" id="demos">
    	<aside class="bg-dark">
	        <div class="container text-center">
	            <div class="call-to-action">
	                <h2>Demos</h2>
	            </div>
	        </div>
	    </aside>
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-lg-4 col-sm-6">
                    <a href="/facebook/home" class="portfolio-box">
                        <img src="${portafolio1Img}" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Demo 1
                                </div>
                                <div class="project-name">
                                    Facebook API client.
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="${portafolio2Img}" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Demo 2
                                </div>
                                <div class="project-name">
                                    No developed yet.
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="${portafolio3Img}" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Demo 3
                                </div>
                                <div class="project-name">
                                    No developed yet.
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="${portafolio4Img}" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Demo 4
                                </div>
                                <div class="project-name">
                                    No developed yet.
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="${portafolio5Img}" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Demo 5
                                </div>
                                <div class="project-name">
                                    No developed yet.
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="${portafolio6Img}" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Demo 6
                                </div>
                                <div class="project-name">
                                    No developed yet.
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Let's Get In Touch!</h2>
                    <hr class="primary">
                    <p>Any doubt? Do you want to collaborate with us? Send us an email!</p>
                </div>
                <div class="col-lg-12 text-center">
                    <i class="fa fa-envelope-o fa-3x wow bounceIn" data-wow-delay=".1s"></i>
                    <p><a href="mailto:javacop@deloitte.ie">javacop@deloitte.ie</a></p>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery -->
    <script src="${jqueryJs}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="${bootstrapMinJs}"></script>

    <!-- Plugin JavaScript -->
    <script src="${easingMinJs}"></script>
    <script src="${fittextJs}"></script>
    <script src="${wowMinJs}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="${creativeJs}"></script>

</body>

</html>
